from flask import jsonify, request
from flask_api import status
from microservicios import app, db  # lectura del archivo __init__
from microservicios.schemadb import Original_content, content, manyContent, Original_Schema

import microservicios.omdb as omdb

@app.route('/netflix/original-content', methods=['GET'])
def getCrud(): 
    all_content = Original_content.query
    if request.args.get('genre'):
        genre = request.args.get('genre')
        all_content = all_content.filter(genre=genre)
    if request.args.get('type'):
        type = request.args.get('type')
        all_content = all_content.filter(type=type)
    if request.args.get('rating'):
        imdb_rating = request.args.get('imdb_rating')
        all_content = all_content.filter(imdb_rating=imdb_rating)
    else:
        all_content = Original_content.query.all()
        all_content = manyContent.dump(all_content)
    return jsonify({'original content': all_content}),\
        status.HTTP_200_OK

@app.route('/netflix/original-content', methods=['POST'])
def postCrud():
    title = request.json['name']
    result = omdb.omdb_values(title)
    name = result['Title']
    genre = result['Genre']
    type = result['Type']
    imdb_rating = result['imdb_rating']

    newContent= Original_content(title,type,genre,imdb_rating)
    
    return jsonify(db.session.add(newContent)),\
        status.HTTP_200_OK


@app.route('/netflix/original-content/<id>', methods=['GET'])
def getContentById(id):
    result = Original_content.query.get(id)
    return Original_Schema.jsonify(result),\
        status.HTTP_200_OK

@app.route('/netflix/original-content/<id>', methods=["PATCH"])
def patchContentById(id):
    content = Original_content.query.get(id)
    name = request.json['name']
    genre = request.json['genre']
    ctype = request.json['type']
    imdb_rating = request.json['imdb_rating']

    content.name = name
    content.genre = genre
    content.type = ctype
    content.imdb_rating = imdb_rating

    db.session.commit()
    return Original_Schema.jsonify(content),\
        status.HTTP_200_OK

@app.route("/content",methods=["GET"])
def getContent():
    if request.args.get("name"):
        title=request.args.get("name"):
        result = omdb.omdb_values(title)
        name = result['Title']
        genre = result['Genre']
        type = result['Type']
        imdb_rating = result['imdb_rating']
        all_content={
            "name":name,
            "genre":genre,
            "type":type,
            "imdb_rating":imdb_rating
        }
        return jsonify({'original content': all_content}),\
        status.HTTP_200_OK

