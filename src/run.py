from microservicios import app


if __name__ == '__main__':
    # Se ejecuta el servicio definiendo el host '0.0.0.0'
    #  para que se pueda acceder desde cualquier IP
    app.run(host='0.0.0.0', port='8084', debug=True)
